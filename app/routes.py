from app import app
from flask import render_template, request

import telegram
from app.credentials import BOT_TOKEN, URL
from app.aibot import get_response

bot = telegram.Bot(token=BOT_TOKEN)

@app.route('/')
def index():
    global bot

    try:
        bot.setWebhook('{URL}{HOOK}'.format(URL=URL, HOOK=BOT_TOKEN))
    except:
        print('failed to set webhook')
        
    return render_template('index.html')

@app.route('/{}'.format(BOT_TOKEN), methods=['POST'])
def respond():
    update = telegram.Update.de_json(request.get_json(force=True), bot)
    chat_id = update.message.chat.id
    msg_id = update.message.message_id

    text = update.message.text.encode('utf-8').decode()
    print('got text message: ', text)

    response = get_response(text)

    bot.sendMessage(chat_id=chat_id, text=response, reply_to_message_id=msg_id)
    return 'ok'