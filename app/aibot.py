import json
import os
import pickle
import random
import numpy as np
import tensorflow as tf
import tflearn as tfl
import nltk
from nltk.stem.lancaster import LancasterStemmer

stemmer = LancasterStemmer()


def load_model():
    global qa_data, labels, processed_words

    qa_location = 'static/model/pairs_QA.json'
    data_location = 'static/model/processed_data.pickle'
    qa_file = os.path.abspath(os.path.join(os.path.dirname(__file__), qa_location))
    data_file = os.path.abspath(os.path.join(os.path.dirname(__file__), data_location))

    with open(qa_file) as f:
        qa_data = json.load(f)
    try:
        with open(data_file, 'rb') as f:
            processed_words, labels, train_set, output_set = pickle.load(f)
    except:
        processed_words, labels, docs_x, docs_y = [], [], [], []

        for intent in qa_data['intents']:
            for pattern in intent['patterns']:
                tokenize_words = nltk.word_tokenize(pattern)
                processed_words.extend(tokenize_words)
                docs_x.append(tokenize_words)
                docs_y.append(intent['tag'])
            if intent['tag'] not in labels:
                labels.append(intent['tag'])
        
        processed_words = [stemmer.stem(w.lower()) for w in processed_words if w not in '?']
        processed_words = sorted(list(set(processed_words)))

        labels.sort()

        train_set = []
        output_set = []

        out_zero = [0] * len(labels)

        for i, doc in enumerate(docs_x):
            bag = []

            stemed_words = [stemmer.stem(w) for w in doc]
            for w in processed_words:
                if w in stemed_words:
                    bag.append(1)
                else:
                    bag.append(0)

            output_list = out_zero[:]
            output_list[labels.index(docs_y[i])] = 1

            train_set.append(bag)
            output_set.append(output_list)

        train_set = np.array(train_set)
        output_set = np.array(output_set)

        with open(data_file, 'wb') as f:
            pickle.dump((processed_words, labels, train_set, output_set), f)
    
    model_location = 'static/model/bot_model.tflearn'
    model_file = os.path.abspath(os.path.join(os.path.dirname(__file__), model_location))

    try:
        tf.reset_default_graph()
        net = tfl.input_data(shape=[None, len(train_set[0])])
        net = tfl.fully_connected(net, 8)
        net = tfl.fully_connected(net, 8)
        net = tfl.fully_connected(net, len(output_set[0]), activation='softmax')
        net = tfl.regression(net)

        model = tfl.DNN(net)
        model.load(model_file)
    except:
        tf.reset_default_graph()
        net = tfl.input_data(shape=[None, len(train_set[0])])
        net = tfl.fully_connected(net, 8)
        net = tfl.fully_connected(net, 8)
        net = tfl.fully_connected(net, len(output_set[0]), activation='softmax')
        net = tfl.regression(net)

        model = tfl.DNN(net)
        model.fit(train_set, output_set, n_epoch=500, batch_size=8, show_metric=True)
        
        model.save(model_file)
        
    return model



def bag_of_words(s):
    bag = [0] * len(processed_words)

    tokenize_words = nltk.word_tokenize(s)
    tokenize_words = [stemmer.stem(w.lower()) for w in tokenize_words]

    for tw in tokenize_words:
        for i, w in enumerate(processed_words):
            if w == tw:
                bag[i] = 1

    return np.array(bag)

bot_model = load_model()

def get_response(question):
    results = bot_model.predict([bag_of_words(question)])[0]

    max_idx = np.argmax(results)
    tag = labels[max_idx]

    if results[max_idx] > 0.6:
        for intent in qa_data['intents']:
            if intent['tag'] == tag:
                responses = intent['responses']
        answer = random.choice(responses)
    else:
        answer = "Oh I see. Nice one"
    return answer

