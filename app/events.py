from flask_socketio import send
from app import socketio
from app.aibot import get_response

@socketio.on('message')
def text(message):
    result = get_response(message)

    send(result)