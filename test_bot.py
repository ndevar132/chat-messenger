from app.aibot import get_response

def chat():
    print("Start taking (type 'exit' to quit)!")
    while True:
        text = input("You: ")
        if text.lower() == "exit":
            break
        print(get_response(text)) 

if __name__ == '__main__':
    chat() 