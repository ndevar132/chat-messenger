# Chat Messenger

Chat messenger is a web application that use an ai chatbot to talk with you.

The chatbot is a contextual chatbot intended to serve you at fast food restaurant. You can try to ask her any question you like and she will to give you her best answer.

![Chat View](images/chat_screen-resized.jpg)

## Feature

The chat apps use Flask-SocketIO to implement real time chat communication.

The chatbot is built using ai, trained using tensorflow module. It is also integrated with telegram, so you can talk with the bot using your telegram app

## Requirement

Before you can run the web apps, you must install all the requirements. You can do this by running this command:

```sh
$ pip install -r requirements.txt
```

Or if you prefer docker, you can build the image and run it

## Usage

The web apps is deployed using docker. You can build the image by running this command:

```sh
$ docker build -t <name of the image> .
```

To run the web apps on your local machine, run this command:

```sh
$ docker run -p 5000:5000 <name of the image>
```

or if you want to test it right away, you could pull and run the image from docker hub using this command:

```sh
$ docker run -p 5000:5000 ndevar132/chat-messenger
```

You can access the application at localhost port 5000

## Configuration

The bot is trained using pairs of question answer defined in file `pairs_QA.json` at directory `/app/static/model/`. You could edit this file to add your own question answer pattern and re-train the bot. To re-train the bot, you must first delete file `processed_data.pickle` and `bot_model.tflearn*`

To connect the chat bot with telegram application, there are some setting to do. You must first build the telegram bot using [BotFather](https://telegram.me/BotFather). To connect the bot with the ai, you must specify your bot token, name of the bot, and the public url of your web apps on `/app/credentials.py`

```sh
BOT_TOKEN = <your bot token>
BOT_NAME = <your bot name>
URL = <public url of your web app>
```

## Test

For testing purpose, I have deployed the web apps on Heroku at [https://web-telegram-ai-chatbot.herokuapp.com/](https://web-telegram-ai-chatbot.herokuapp.com/).
To test the telegram integration with the chatbot, you could access the bot at @DiyCuteBot

**Note:** If the bot on the apps or telegram does not respond to the chat, you have to refresh the web page. This is happened because of heroku policy to make apps sleeping if there is no traffic within 30 minutes. [see this](https://devcenter.heroku.com/articles/free-dyno-hours)

## License

[MIT](https://choosealicense.com/licenses/mit/)
