FROM python:3.6-slim

RUN apt-get update
RUN apt-get install gcc -y

COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY nltk_data /usr/local/nltk_data
COPY . /app

ENTRYPOINT flask run -h 0.0.0.0 -p 5000